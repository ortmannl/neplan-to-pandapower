#### (c) Copyright <2022> <ETH Zurich, Lukas Ortmann, Christian Rubin>
# Use the files in this folder to convert your power net from a simpow file into a pandapower power net


## Quick use:
- export your net in NEPLAN (Datei->Export->Simpow...->Ok)
- rename the exported file into net.txt
- copy net.txt into this folder
- run neplantopandapower.py
- controll the conversion using the controll_conversion.ipynb file

## What you get when running:
You end up with a file called pp_net.xlsx. It is an exel file containing your net as a pandapower net.
To use the net in your code, simply use "net = pp.from_excel('pp_net.xlsx')" in your python code.

## What the files do:
#### ntopfunctions.py
contains the necessary functions for the individual elements
#### neplantopandapoer.py
if executed, converts your file
#### control_conversion.ipynb
a jupyter notebook to controll and troubleshoot the conversion

## Known problems / Limitations:
- To export the net in NEPLAN, no name is allowed to have a space. 
- When running a power flow with your pandapower net, the voltages may not be the same as in the NEPLAN power flow.
  This is likely due to the missing tap changer settings - that can not be converted and has to be added manually.
  The easiest way to do so, is to directly enter the values into the excel file.

The developed net conversion is based on the model we were provided with. This possibly leads
to missing elements in the conversion if another net gets converted even if we converted a, in
terms of nodes, large distribution grid.
We further only needed to perform a power flow, which causes the needed values for an optimal
power flow to be missed in the toolbox.
Additionally, the .simpower file also does not provide geo-data, which would be needed for a
useful plot of the network.
For these reasons we highly recommend checking the net closely for plausibility after using our
net converter.


## Below you find details about what gets converted

### Bus
Busses are the nodes of the network to which all other elements connect. The conversion of the
busses can be done straightforwardly, given all values in the .simpower file.
The toolbox adds the following properties per bus to the pandapower net:
- The bus voltage level vn_kv
- The name of the bus name
- Set in_service = True

### Line
Lines connect busses but in this case, not all values are given in the .simpower file. However the
capacitance c_nf_per_km can be calculated out of the given susceptance B (the imaginary
part of the admittance).
The toolbox adds the following properties per line to the pandapower net:
- The name of the connected busses f rom_bus and to_bus
- The length of the line length_km
- The resistance of the line per km r_ohm_per_km
- The inductance of the line per km x_ohm_per_km
- The maximal thermal current max_i_ka and set the corresponding scaling factor df = 1
- The specification whether a line is in service or not in_service = T rue/F alse
- Set the conductance g_us_per_km = 0
- Set the number of parallel line system parallel = 1
- The capacitance of the line c_nf_per_km with
c_nf_per_km =
B ∗ 10^9/2πf

#### Notes to Converting of Lines
In the model we were provided with, all conductances of the lines are equal to 0, which is why
the toolbox sets g_us_per_km = 0.
This has to be changed in the code of the toolbox if one wants to also convert a conductance
value.
Further the toolbox sets max_i_ka = 1 if we get zero since this is not allowed in pandapower.
If a very short line (R < 0.005Ω or X < 0.005Ω) gets converted, the toolbox will add a switch
instead of a line since a power flow would otherwise not converge in pandapower.


### Switch
Switches in our model only connect busses with a switching state closed = T rue/F alse what
can be read out of the .simpower file.
The toolbox adds the following properties per switch to the pandapower net:
- The name of the connected busses bus and element
- Set the element type the switch connects to et = b
- The switching state of the switch closed

#### Notes to Converting of Switches
Since we only have bus-to-bus switches in our model, we set the element type et = b.
This has to be changed in the code of the toolbox if one wants to add other types of switches to
the pandapower net.


### Load
The toolbox adds the following properties per load to the pandapower net:
- The name of the connected bus bus
- The active power of the load p_mw
- The reactive power of the load q_mvar
- Set const_z_percent = const_i_percent = 0
- Set the scaling factor for active and reactive power scaling = 1
- Set in_service = True
- Set type = wye

#### Notes to Converting of Loads
The toolbox sets const_z_percent = const_i_percent = 0, scaling = 1 and type = wye
because it can not be obtained out of the provided file.
This has to be changed in the already converted pandapower net if one wants to add other types
of loads to the pandapower net.


### Transformer
There are two-winding and three-winding transformers to convert. Below we describe the conversion of a two-winding transformer. The conversion of the three-winding transformer works
similar.
The toolbox adds the following properties per two-winding transformer to the pandapower net:
- The name of the connected busses hv_bus and lv_bus
- The rated power of the transformer sn_mva
- The rated voltages on the high and low voltage bus vn_hv_kv and vn_lv_kv
- The minimum and maximum tap position tap_min and tap_max if available
- The tap step size tap_step_percent if available
- The short circuit voltage vk_percent % of vn_hv_kv (called ex12 in the .simpower
file)
- The real component of the short circuit voltage vkr_percent in % of sn (called er12 in
the .simpower file)
- Set the iron losses pfe_kw = 0
- Set the open loop losses i0_percent = 0
- Set the transformer phase shift angle shif t_degree = 0
- Set in_service = True/False

#### Notes to Converting of Transformers
Even if the tap steps are added. The actual position of the tap changers does not get exported
from the NEPLAN® model to the .simpower file. This leads to unexpected results when running
a power flow.
It is, therefore, necessary to manually insert the positions in the excel file, where the pandapower
net is saved.

### Shunt
The toolbox adds the following properties per shunt to the pandapower net:
- The name of the connected bus bus
- The active power p_mw
- The reactive power q_mvar

### Feeder / External Grid
Connections to an external grid are called feeders in the .simpower file.
The toolbox adds the following properties per external grid to the pandapower net:
- The name of the connected bus bus
- The voltage at the node per unit vm_pu with
vm_pu = Ufeeder/Ubus
- The name of the external grid name
- Set the angle set point va_degree = 0
- Set in_service = True
































