## (c) Copyright <2022> <ETH Zurich, Lukas Ortmann, Christian Rubin>
# simpow net net.txt get converted into pp_net.xlsx
#import everything needed
import pandapower as pp
from ntopfunctions import *

if __name__ == '__main__':

    net = pp.create_empty_network()

    ## open the .txt file and read the lines
    with open('net.txt') as f:
        read = f.readlines()

    value = ''

    ##mode initialization
    nodes = False
    lines = False
    switch = False
    loads = False
    transformers2w = False
    transformers3w = False
    reactors = False
    capacitors = False 
    shunts = False
    hvdc = False
    converter = False
    asyncma = False
    tablesshunts = False
    mloads = False
    tablesasync = False
    instr = False
    instr2W = False
    instr3W = False

    ## needed variables
    l1 = False
    l2 = False
    l3 = False
    l4 = False
    l5 = False
    line1 = ''
    line2 = ''
    line3 = ''
    line4 = ''
    line5 = ''


    for line in read:
        #first we need to know the mode
        if line.strip() == 'NODES':
            nodes = True
            continue
        if line.strip() == 'LINES':
            lines = True
            continue
        if line.strip() == 'LOADS':
            loads = True
            continue
        if line.strip() == 'TRANSFORMERS':
            transformers2w = True
            continue
        if line.strip() == 'SREACTORS':
            #not used for our net
            reactors = True
            continue
        if line.strip() == 'SCAPACITORS':
            #not used for our net
            capacitors = True
            continue
        if line.strip() == 'SHUNT IMPEDANCES':
            shunts = True
            continue
        if line.strip() == 'POWER INSTRUCTIONS':
            instr = True
            continue

        # 'END' will always reset all modes 
        elif line.strip() == 'END':
            nodes = False
            lines = False
            switch = False
            loads = False
            transformers2w = False
            transformers3w = False
            reactors = False
            capacitors = False 
            shunts = False
            instr = False
            instr2W = False
            instr3W = False
            l1 = False
            l2 = False
            line1 = ''
            line2 = ''

        if nodes:
            linetonode(line.strip(),net)
            continue
            
        if lines:
            if l1:
                l1 = False
                l2 = True
                line2 = line.strip()
            elif l2:
                l2 = False
                linetoline(line1,line2,  line.strip(), net)
                    
            else:
                l1 = True
                line1 = line.strip()
                if line1 == '!!!! Buscoupler':
                    l1 = False
                    line1 = ''
                    lines = False
                    switch = True
            
            continue
        
        if switch:
            if line.strip() == '!!!! Disconnect Switches' or line.strip() == '!!!! Load Switches' or line.strip() == '!!!! Circuit Breaker':
                continue
            linetoswitch(line.strip(),net)
        
        
        
        if loads:
            linetoload(line.strip(), net)
            continue
            
        if transformers2w:
            if l1:
                l1 = False
                l2 = True
                line2 = line.strip()
            elif l2:
                l2 = False
                linetotrans2w(line1,line2,  line.strip(), net)
                    
            else:
                l1 = True
                line1 = line.strip()
                if line1 == '!!!! 2W-Transformers':
                    l1 = False
                elif line1 == '!!!! 3W-Transformers':
                    transformers2w = False
                    transformers3w = True
                    l1 = False
                    l2 = False
                    line1 = ''
                    line2 = ''
    
            continue
        
        if transformers3w:
            if l1:
                l1 = False
                l2 = True
                line2 = line.strip()
            elif l2:
                l2 = False
                l3 = True
                line3 = line.strip()
            elif l3:
                l3 = False
                l4 = True
                line4 = line.strip()
            elif l4:
                l4 = False
                l5 = True
                line5 = line.strip()
            elif l5:
                l5 = False
                linetotrans3w(line1,line2,line3, line4, line5,  line.strip(), net)
                    
            else:
                l1 = True
                line1 = line.strip()
    
            continue
        if shunts:
            linetoshunt(line, net)
            continue
            
        if instr:
            if l1:
                l1 = False
                linetofeeder(line1, line.strip(),net)
            else:
                l1 = True
                line1 = line.strip()
                if line1 == '!!!! Power Instructions (SM)':
                    l1 = False
                elif line1 == '!!!! Power Instructions (SVC)':
                    l1 = False
                elif line1 == '!!!! Power Instructions (FEEDER)':
                    l1 = False
                elif line1 == '!!!! Power Instructions (2W-TRAFO)':
                    l1 = False
                    instr = False
                    instr2W = True
            continue
        
        if instr2W:
            if l1:
                l1 = False
                #function not yet implemented
                #lineto2wtrafo(line1, line.strip(),net)
            else:
                l1 = True
                line1 = line.strip()
                if line1 == '!!!! Power Instructions (3W-TRAFO)':
                    l1 = False
                    instr2W = False
                    instr3W = True
            continue
        
        if instr3W:
            if l1:
                l1 = False
                #function not yet implemented 
                #lineto3Wtrafo(line1, line.strip(),net)
            else:
                l1 = True
                line1 = line.strip()
                if line1 == '!!!! Power Instructions (HVDC)':
                    l1 = False
                    instr3W = False

    pp.to_excel(net,'pp_net.xlsx', include_empty_tables=False, include_results=False)



