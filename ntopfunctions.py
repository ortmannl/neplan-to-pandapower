# (c) Copyright <2022> <ETH Zurich, Lukas Ortmann, Christian Rubin>
#import everything needed
import pandapower as pp
import math

## strtofl() takes a string with an equal sign (as example UB=16.00000) and returns the string number
## after the equal sign as a float number
def strtofl(string):
    eq = False
    result = ''
    for i in string:
        if eq:
            result += i
        elif i == '=':
            eq = True
        else:
            continue   
    if result == '':
        return 0.0
    else:
        return float(result)  

## exertovkr computes vkr_percent out of ex and er value 
## vkr_percent is the real component of short circuit voltage

def exertovkr(ex, er):
    ex = ex*100
    er = er*100
    return ex*math.cos(math.atan(math.sqrt(ex**2-er**2)/er))
            
        
### linetonode() takes a string of a line and adds the node to the pandapower network
### exampleline = ' ANYNAME UB=16.00000 AREA=B    REGION=A   '
def linetonode(linestring,net):
    #modes init
    reset = True
    name = False
    ub = False
    area = False
    region = False
    #values init
    name_res = ''
    ub_res = ''
    area_res = ''
    region_res = ''

    for x in linestring:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if lastcharblan == True)
            if reset:
                reset = False
                if name:
                    name = False
                    ub = True
                elif ub:
                    ub = False
                    area = True
                elif area:
                    area = False
                    region = True
                elif region:
                    region = False
                else:
                    name = True       
            #now we add the characters       
            if name:
                name_res += x
            elif ub:
                ub_res += x
            elif area:
                area_res += x
            elif region:
                region_res += x  

    ## after we iterated over the string - the bus can be added to the pandapower net with ub and the name
    pp.create_bus(net, strtofl(ub_res), name=name_res, in_service=True)

## linetoline() takes 3 strings (lines) and adds a line to the pandapower network
## examlelines =  ANYNAME1 ANYNAME2
##                NO=1     TYPE=2  NCON=1  L=0.470000
##                R=0.193000 X=0.114000 B=0.000107  I1MAX=1.000 
def linetoline(line1, line2, line3, net):
    #modes init
    reset = True
    name_from = False
    name_to = False
    no = False
    lintype = False
    ncon = False
    l = False
    r = False
    x_value = False
    b = False
    imax = False
    service = False
    #values init
    name_from_res = ''
    name_to_res = ''
    no_res = '0'
    lintype_res = '0'
    ncon_res = ''
    l_res = '0'
    r_res = '0'
    x_res = '0'
    b_res = '0'
    imax_res = '1'

    for x in line1:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if name_from:
                    name_from = False
                    name_to = True
                elif name_to:
                    name_to = False
                else:
                    name_from = True
            #now we add the characters       
            if name_from:
                name_from_res += x
            elif name_to:
                name_to_res += x

    reset = True
    for x in line2:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if no:
                    no = False
                    lintype = True
                elif lintype:
                    lintype = False
                    ncon = True
                elif ncon:
                    ncon = False
                    l = True
                elif l:
                    l = False
                else:
                    no = True       
            #now we add the characters         
            if no:
                no_res += x
            elif lintype:
                lintype_res += x
            elif ncon:
                ncon_res += x
            elif l:
                l_res += x  
    reset = True
    for x in line3:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if r:
                    r = False
                    x_value = True
                elif x_value:
                    x_value = False
                    b = True
                elif b:
                    b = False
                    imax = True
                elif imax:
                    imax = False
                else:
                    r = True       
            #now we add the characters        
            if r:
                r_res += x
            elif x_value:
                x_res += x
            elif b:
                b_res += x
            elif imax:
                imax_res += x  

    #calculate c out of b (model in Neplan Help)  
    c = strtofl(b_res)*1000000000/(100*math.pi)
    
    ## problem with imax_res = 0 is not allowed in pandapower
    if strtofl(imax_res) == 0.0:
        imax_res = 'imax=1.0'


    ## IF NCON=1 NOT IN SERVICE
    if ncon_res == 'NCON=1':
        service = False
    else:
        service = True
    
    
    ### IF LINE IS LONGER THAN 0.04 CREATE LINE OTHERWISE SWITCH (ALLWAYS CLOSED) - KNOWN PROBLEM OF PANDAPOWER
    
    if strtofl(l_res)*strtofl(r_res) <= 0.005 or strtofl(l_res)*strtofl(x_res) <= 0.005:
        try:
            pp.create_switch(net, bus=pp.get_element_index(net, 'bus', name_from_res), element=pp.get_element_index(net, 'bus', name_to_res),
                             et='b', closed=service)
        except:
            print('Line (switch) from:',name_from_res, ' to ', name_to_res, 'not added')
    else:
        try:
            ## after we iterated over the strings - the line can be added to the pandapower net with names to and from, length, r, x, c and maxi
            pp.create_line_from_parameters(net, from_bus=pp.get_element_index(net, 'bus', name_from_res), 
                                           to_bus=pp.get_element_index(net, 'bus', name_to_res),
                                           length_km=strtofl(l_res), r_ohm_per_km=strtofl(r_res) , x_ohm_per_km=strtofl(x_res) ,
                                           c_nf_per_km=c , max_i_ka=strtofl(imax_res), in_service=service,df=1.0, parallel=1,
                                           g_us_per_km=0.0)
        except:
            print('Line from:',name_from_res, ' to ', name_to_res, 'not added')

### linetoswitch() takes a line as a string and adds a switch to the pandapower network
### exampleline = ANYNAME1 ANYNAME2 NO=1    TYPE=0 NCON=0 
def linetoswitch(linestring,net):
    #modes init
    reset = True
    name_from = False
    name_to = False
    no = False
    typen = False
    ncon = False
    service = False
    #values init
    name_from_res = ''
    name_to_res = ''
    no_res = ''
    typen_res = ''
    ncon_res = ''

    for x in linestring:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if name_from:
                    name_from = False
                    name_to = True
                elif name_to:
                    name_to = False
                    no = True
                elif no:
                    no = False
                    typen = True
                elif typen:
                    typen = False
                    ncon = True
                elif ncon:
                    ncon = False
                else:
                    name_from = True       
            #now we add the characters
            if name_from:
                name_from_res += x
            elif name_to:
                name_to_res += x
            elif no:
                no_res += x
            elif typen:
                typen_res += x 
            elif ncon:
                ncon_res += x
    if ncon_res == 'NCON=1':
        service = False
    else:
        service = True

    try:
        ## after we iterated over the string - the swich can be added to the pandapower net names from and to
        pp.create_switch(net, bus=pp.get_element_index(net, 'bus', name_from_res),
                         element=pp.get_element_index(net, 'bus', name_to_res),et='b', closed=service)
    except:
        print('Switch from:',name_from_res, ' to ', name_to_res, 'not added')

### Take line and add load to the pandapower network   
### exampleline = ANYNAME NO=1    NCON=0   P=0.067    Q=0.020 
def linetoload(linestring,net):
    #modes init
    reset = True
    node = False
    no = False
    ncon = False
    p = False
    q = False
    #values init
    node_res = ''
    no_res = ''
    ncon_res = ''
    p_res = '0'
    q_res = '0'

    for x in linestring:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if node:
                    node = False
                    no = True
                elif no:
                    no = False
                    ncon = True
                elif ncon:
                    ncon = False
                    p = True
                elif p:
                    p = False
                    q = True
                elif q:
                    q = False
                else:
                    node = True       
            #now we add the characters     
            if node:
                node_res += x
            elif no:
                no_res += x
            elif ncon:
                ncon_res += x
            elif p:
                p_res += x 
            elif q:
                q_res += x
    ## after we iterated over the string - the load can be added to the pandapower net with q,p and the name of the bus            
    pp.create_load(net, bus=pp.get_element_index(net, 'bus', node_res), p_mw=strtofl(p_res), q_mvar=strtofl(q_res), 
                   const_z_percent=0,const_i_percent=0, scaling=1.0, in_service=True, type='wye')

## Take 3 lines and add a transformator to the pandapower network
### Examplelines:       ANYNAME1 ANYNAME2 NO=1    NCON=0 
###                     SN=40.000000 UN1=18.000000 UN2=120.800000 ER12=0.0023   EX12=0.1323  
###                     FI=0.0      -NSTEP=1      +NSTEP=23     STEP=-0.0099
def linetotrans2w(line1, line2, line3, net):
    #modes init
    reset = True
    name_from = False
    name_to = False
    no = False
    ncon = False
    sn = False
    un1 = False
    un2 = False
    er12 = False
    ex12 = False
    fi = False
    nsteplow = False
    nstephigh = False
    step = False
    service = False
    #values init
    name_from_res = ''
    name_to_res = ''
    no_res = ''
    ncon_res = ''
    sn_res = '0'
    un1_res = '0'
    un2_res = '0'
    er12_res = '0'
    ex12_res = '0'
    fi_res = '0'
    nsteplow_res = '0'
    nstephigh_res = '0'
    step_res = '0'


    for x in line1:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if name_to:
                    name_to = False
                    name_from = True
                elif name_from:
                    name_from = False
                    no = True
                elif no:
                    no = False
                    ncon = True
                elif ncon:
                    ncon = False
                else:
                    name_to = True       
            #now we add the characters      
            if name_to:
                name_to_res += x
            elif name_from:
                name_from_res += x
            elif no:
                no_res += x
            elif ncon:
                ncon_res += x
    reset = True
    for x in line2:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if sn:
                    sn = False
                    un1 = True
                elif un1:
                    un1 = False
                    un2 = True
                elif un2:
                    un2 = False
                    er12 = True
                elif er12:
                    er12 = False
                    ex12 = True
                elif ex12:
                    ex12 = False
                else:
                    sn = True       
            #now we add the characters       
            if sn:
                sn_res += x
            elif un1:
                un1_res += x
            elif un2:
                un2_res += x
            elif er12:
                er12_res += x  
            elif ex12:
                ex12_res += x   
    reset = True
    for x in line3:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if fi:
                    fi = False
                    nsteplow = True
                elif nsteplow:
                    nsteplow = False
                    nstephigh = True
                elif nstephigh:
                    nstephigh = False
                    step = True
                elif step:
                    step = False
                else:
                    fi = True       
            #now we add the characters       
            if fi:
                fi_res += x
            elif nsteplow:
                nsteplow_res += x
            elif nstephigh:
                nstephigh_res += x
            elif step:
                step_res += x 
                
    ## after we iterated over the three strings - the transformer can be added to the pandapower net 
    if ncon_res == 'NCON=1':
        service = False
    else:
        service = True
    try:
        pp.create_transformer_from_parameters(net, hv_bus=pp.get_element_index(net, 'bus', name_from_res),
                                              lv_bus=pp.get_element_index(net, 'bus', name_to_res),
                                              sn_mva=strtofl(sn_res), vn_hv_kv=strtofl(un2_res),
                                              vn_lv_kv=strtofl(un1_res), vkr_percent=strtofl(er12_res)*100,
                                              vk_percent=strtofl(ex12_res)*100, pfe_kw=0., i0_percent=0.,
                                              shift_degree=0, tap_side=None, tap_max=strtofl(nstephigh_res),
                                              tap_min=strtofl(nsteplow_res), tap_step_percent=strtofl(step_res)*100,
                                              tap_phase_shifter=False,in_service=service, name=None, vector_group=None,
                                              index=None,parallel=1, df=1.0,
                                              pt_percent=None, oltc=None, tap_dependent_impedance=None,
                                              vk_percent_characteristic=None, vkr_percent_characteristic=None,
                                              xn_ohm=None)
        
    except:
        print('Could not create transformer on buses:', name_from_res, 'to', name_to_res)

### Take 6 lines and add a transformator to the pandapower network
### Examplelines:   Anyname1 Anyname2 Anyname3 NW=3 NO=1    NCON=0 
###                 SN=20.000   UN1=52.500000 UN2=18.000000 UN3=8.100000
###                 ER12=0.0039   EX12=0.1096   ER23=0.0039   EX23=0.0411  
###                 ER13=0.0039   EX13=0.0576  
###                 FI12=0.0      FI13=150.0    -N2STEP=1      +N2STEP=23     2STEP=0.0122
###                 -N3STEP=1      +N3STEP=1      3STEP=0.0476
def linetotrans3w(line1, line2, line3, line4, line5, line6, net):
    #modes init
    reset = True
    name1 = False
    name2 = False
    name3 = False
    nw = False
    no = False
    ncon = False
    sn=False
    un1=False
    un2=False
    un3=False
    er12=False
    ex12=False
    er23=False
    ex23=False
    er13=False
    ex13=False
    fi12=False
    fi13=False
    n2steplow=False
    n2stephigh=False
    step2=False
    n3steplow=False
    n3stephigh=False
    step3=False
    
    #values init
    name1_res = ''
    name2_res = ''
    name3_res = ''
    nw_res = ''
    no_res=''
    ncon_res = ''
    sn_res = '0'
    un1_res = '0'
    un2_res = '0'
    un3_res = '0'
    er12_res = '0'
    ex12_res = '0'
    er23_res = '0'
    ex23_res = '0'
    er13_res = '0'
    ex13_res = '0'
    fi12_res = '0'
    fi13_res = '0'
    n2steplow_res = '0'
    n2stephigh_res = '0'
    step2_res = '0'
    n3steplow_res = '0'
    n3stephigh_res = '0'
    step3_res = '0'


    for x in line1:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if name1:
                    name1 = False
                    name2 = True
                elif name2:
                    name2 = False
                    name3 = True
                elif name3:
                    name3 = False
                    nw = True
                elif nw:
                    nw = False
                    no = True
                elif no:
                    no = False
                    ncon = True
                elif ncon:
                    ncon = False
                else:
                    name1 = True       
            #now we add the characters      
            if name1:
                name1_res += x
            elif name2:
                name2_res += x
            elif name3:
                name3_res += x
            elif nw:
                nw_res += x
            elif no:
                no_res += x
            elif ncon:
                ncon_res += x
    reset = True
    for x in line2:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if sn:
                    sn = False
                    un1 = True
                elif un1:
                    un1 = False
                    un2 = True
                elif un2:
                    un2 = False
                    un3 = True
                elif un3:
                    un3 = False
                else:
                    sn = True       
            #now we add the characters       
            if sn:
                sn_res += x
            elif un1:
                un1_res += x
            elif un2:
                un2_res += x
            elif un3:
                un3_res += x

    reset = True
    for x in line3:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if er12:
                    er12 = False
                    ex12 = True
                elif ex12:
                    ex12 = False
                    er23 = True
                elif er23:
                    er23 = False
                    ex23 = True
                elif ex23:
                    ex23 = False
                else:
                    er12 = True       
            #now we add the characters      
            if er12:
                er12_res += x
            elif ex12:
                ex12_res += x
            elif er23:
                er23_res += x
            elif ex23:
                ex23_res += x  
    reset = True
    for x in line4:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if er13:
                    er13 = False
                    ex13 = True
                elif ex13:
                    ex13 = False
                else:
                    er13 = True       
            #now we add the characters        
            if er13:
                er13_res += x
            elif ex13:
                ex13_res += x
    reset = True
    for x in line5:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if fi12:
                    fi12 = False
                    fi13 = True
                elif fi13:
                    fi13 = False
                    n2steplow = True
                elif n2steplow:
                    n2steplow = False
                    n2stephigh = True
                elif n2stephigh:
                    n2stephigh = False
                    step2 = True
                elif step2:
                    step2 = False
                else:
                    fi12 = True       
            #now we add the characters     
            if fi12:
                fi12_res += x
            elif fi13:
                fi13_res += x
            elif n2steplow:
                n2steplow_res += x
            elif n2stephigh:
                n2stephigh_res += x
            elif step2:
                step2_res += x
    reset = True
    for x in line6:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if n3steplow:
                    n3steplow = False
                    n3stephigh = True
                elif n3stephigh:
                    n3stephigh = False
                    step3 = True
                elif step3:
                    step3 = False
                else:
                    n3steplow = True       
            #now we add the characters       
            if n3steplow:
                n3steplow_res += x
            elif n3stephigh:
                n3stephigh_res += x
            elif step3:
                step3_res += x     
  
    ## after we iterated over the 6 lines string - the trafo can be added to the pandapower net  
    try:
        pp.create_transformer3w_from_parameters(net, hv_bus=pp.get_element_index(net, 'bus', name1_res),
                                                mv_bus=pp.get_element_index(net, 'bus', name2_res),
                                                lv_bus=pp.get_element_index(net, 'bus', name3_res),
                                                vn_hv_kv=strtofl(un1_res), vn_mv_kv=strtofl(un2_res),
                                                vn_lv_kv=strtofl(un3_res), sn_hv_mva=strtofl(sn_res),
                                                sn_mv_mva=strtofl(sn_res), sn_lv_mva=strtofl(sn_res),
                                                vk_hv_percent=strtofl(ex12_res)*100, vk_mv_percent=strtofl(ex23_res)*100,
                                                vk_lv_percent=strtofl(ex13_res)*100, vkr_hv_percent=strtofl(er12_res)*100,
                                                vkr_mv_percent=strtofl(er23_res)*100, vkr_lv_percent=strtofl(er13_res)*100,
                                                pfe_kw=0., i0_percent=0., shift_mv_degree=0.0, shift_lv_degree=0.0,
                                                tap_side=None, name=None, in_service=True, index=None, tap_at_star_point=False,
                                                vector_group=None, tap_dependent_impedance=None, vk_hv_percent_characteristic=None,
                                                vkr_hv_percent_characteristic=None, vk_mv_percent_characteristic=None,
                                                vkr_mv_percent_characteristic=None, vk_lv_percent_characteristic=None,
                                                vkr_lv_percent_characteristic=None)

    except:
        print('Could not create transformer on buses:', name1_res, 'to', name2_res, 'and', name3_res)
        
    
    
### take line and add a shunt to the pandapower network    
### Exampleline:  Anyname NO=2    NCON=0   UN=16.000000 P=0.000001 Q=-0.107000
def linetoshunt(linestring, net):
    #modes init
    reset = True
    node = False
    no = False
    ncon = False
    un = False
    p = False
    q = False
    #values init
    node_res = ''
    no_res = ''
    ncon_res = ''
    un_res = '0'
    p_res = '0'
    q_res = '0'

    for x in linestring:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if lastcharblan == True)
            if reset:
                reset = False
                if node:
                    node = False
                    no = True
                elif no:
                    no = False
                    ncon = True
                elif ncon:
                    ncon = False
                    un = True
                elif un:
                    un = False
                    p = True
                elif p:
                    p = False
                    q = True
                elif q:
                    q = False
                else:
                    node = True       
            #now we add        
            if node:
                node_res += x
            elif no:
                no_res += x
            elif ncon:
                ncon_res += x
            elif un:
                un_res += x
            elif p:
                p_res += x 
            elif q:
                q_res += x
    pp.create_shunt(net, pp.get_element_index(net, 'bus', node_res), q_mvar=strtofl(q_res), p_mw=strtofl(p_res))

### Take 2 lines and add a feeder to the pandapower network
### Examplelines:  Anyname NAME=NAME NCON=0  TYPE=NODE RTYP=SW
###                U=50.000   FI=0.000   
def linetofeeder(line1, line2, net):
    #modes init
    reset = True
    node = False
    name = False
    ncon = False
    typen = False
    rtyp = False
    u = False
    fi = False
    
    #values init
    node_res = ''
    name_res = ''
    ncon_res = ''
    typen_res = ''
    rtyp_res = ''
    u_res = '0'
    fi_res = '0'


    for x in line1:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if node:
                    node = False
                    name = True
                elif name:
                    name = False
                    ncon = True
                elif ncon:
                    ncon = False
                    typen = True
                elif typen:
                    typen = False
                    rtyp = True
                elif rtyp:
                    rtyp = False
                else:
                    node = True       
            #now we add the characters   
            if node:
                node_res += x
            elif name:
                name_res += x
            elif ncon:
                ncon_res += x
            elif typen:
                typen_res += x
            elif rtyp:
                rtyp_res += x
    reset = True
    for x in line2:
        #everytime there is a space -> mode is changed and nothing else happens
        if x == ' ':
            reset = True

        else:      
            #first we have to decide in which mode we are (only if reset == True)
            if reset:
                reset = False
                if u:
                    u = False
                    fi = True
                elif fi:
                    fi = False
                else:
                    u = True       
            #now we add the characters       
            if u:
                u_res += x
            elif fi:
                fi_res += x   

    ## after we iterated over the string - the grid can be added to the pandapower net
    busid = pp.get_element_index(net, 'bus', node_res)
    busvolt = net.bus.loc[busid].at['vn_kv']
    vmpu =  strtofl(u_res)/busvolt       
    pp.create_ext_grid(net, bus=busid, vm_pu=vmpu, va_degree=0.0, name=name_res, in_service=True)
    
